package cake;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.Test;

import cake.Cake;
import cake.ChocolateSprinkles;
import cake.DecoratorElement;
import cake.Element;
import cake.LoafPanCake;
import cake.MuffinCake;
import cake.RoundCake;
import cake.TextWriting;
import cake.WhippedCream;

public class CakeFactoryTest {

	@Test
	public void testRoundCake() throws Exception {

		Cake cake = new RoundCake();
		// base cake price
		assertEquals(cake.getDescription(), 21, cake.cost(), 0);

		cake = new WhippedCream(cake); // 4.50
		cake = new ChocolateSprinkles(cake); // 2.50
		cake = new DecoratorElement(cake, Element.STAR); // 4.00

		assertEquals("Round cake description", "round, multi-layered cake, Whipped Cream, Chocolate Sprinkles, STAR",
				cake.getDescription());
		// cake price with decorators
		assertEquals(cake.getDescription(), 32, cake.cost(), 0);
	}

	@Test
	public void testLoafPanCake() throws Exception {

		Cake cake = new LoafPanCake(); // 16
		cake = new DecoratorElement(cake, Element.HEART); // 4
		cake = new DecoratorElement(cake, Element.FIR); // 4
		cake = new TextWriting(cake, "Farewell"); // 10

		assertEquals("Loaf Pan Cake", "loaf pan cake, HEART, FIR, Farewell", cake.getDescription());
		assertEquals(cake.getDescription(), 34, cake.cost(), 0);
	}

	@Test
	public void testMuffinCake() {

		Cake cake = new MuffinCake(); // 19
		cake = new WhippedCream(cake); // 4.50
		cake = new ChocolateSprinkles(cake); // 2.50
		cake = new TextWriting(cake, "Anniversary"); // 8

		assertEquals("muffin cake description",
				"6-set bilberry muffin cake, Whipped Cream, Chocolate Sprinkles, Anniversary with ink on a card",
				cake.getDescription());
		assertEquals(cake.getDescription(), 34, cake.cost(), 0);
	}

	@Test
	public void testMuffinCakeWithBirthdayText() {

		Cake cake = new MuffinCake(); // 19
		cake = new TextWriting(cake, "Geburtstag"); // 8

		assertEquals("muffin cake description", "6-set bilberry muffin cake, Geburtstag with ink on a card",
				cake.getDescription());
		assertEquals(cake.getDescription(), 19, cake.cost(), 0);
	}

	@Test
	public void testDiscount() {

		Cake cake = new MuffinCake(); // 19
		cake.setCreatedDate(LocalDate.now().minus(3, ChronoUnit.DAYS));

		assertEquals(cake.getDescription(), 15.2, cake.cost(), 0.1);
	}

	@Test(expected = Exception.class)
	public void testDuplicateDecoratorElements() throws Exception {

		Cake cake = new LoafPanCake(); // 16
		cake = new DecoratorElement(cake, Element.HEART); // 4
		cake = new DecoratorElement(cake, Element.HEART); // 4
	}
	
	@Test
	public void testIsInnerCakeType() throws Exception {

		Cake cake = new MuffinCake();
		cake = new DecoratorElement(cake, Element.HEART); // 4
		cake = new DecoratorElement(cake, Element.FIR); // 4
		cake = new TextWriting(cake, "Farewell"); // 10

		assertEquals("Muffin with decoration elements", "6-set bilberry muffin cake, HEART, FIR, Farewell with ink on a card", cake.getDescription());
	}

}
