package cake;

public class ChocolateSprinkles extends CakeDecorator {

	public ChocolateSprinkles(Cake cake) {
		super(cake, "Chocolate Sprinkles", 2.5);
	}
}
