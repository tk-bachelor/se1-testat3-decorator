package cake;

public class DecoratorElement extends CakeDecorator {

	public DecoratorElement(Cake cake, Element element) throws Exception {
		super(cake, validateElement(cake, element), 4);
	}

	private static String validateElement(Cake cake, Element element) throws Exception {
		if (cake.getDescription().contains(element.toString())) {
			throw new Exception("Duplicate Decorator Element: " + element.toString());
		} else {
			return element.toString();
		}
	}

}