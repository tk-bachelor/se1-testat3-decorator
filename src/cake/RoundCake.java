package cake;

public class RoundCake extends Cake {
	
	protected String description = "round, multi-layered";
	
	@Override
	public String getDescription() {
		return String.format("%s %s", this.description, super.getDescription());
	}

	@Override
	public double baseCost() {
		return 21;
	}

}
