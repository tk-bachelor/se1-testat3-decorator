package cake;

public class LoafPanCake extends Cake {

	protected String description = "loaf pan";

	@Override
	public String getDescription() {
		return String.format("%s %s", this.description, super.getDescription());
	}

	@Override
	public double baseCost() {
		return 16;
	}

}
