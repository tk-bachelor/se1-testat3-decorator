package cake;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public abstract class Cake {
	protected String description = "cake";
	private LocalDate createdDate;

	public Cake() {
		this.createdDate = LocalDate.now();
	}

	public String getDescription() {
		return description;
	}

	public final double cost() {
		LocalDate twoDaysAgo = LocalDate.now().minus(2, ChronoUnit.DAYS);

		double baseCost = baseCost();
		return createdDate.isBefore(twoDaysAgo) ? baseCost * 0.8 : baseCost;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	protected abstract double baseCost();
}
