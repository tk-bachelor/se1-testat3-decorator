package cake;

public class MuffinCake extends Cake {

	protected String description = "6-set bilberry muffin";

	@Override
	public String getDescription() {
		return String.format("%s %s", this.description, super.getDescription());
	}

	@Override
	public double baseCost() {
		return 19;
	}

}
