package cake;

public class TextWriting extends CakeDecorator {
	protected String text;
	private final String BIRTHDAY = "Geburtstag";

	public TextWriting(Cake cake, String text) {
		super(cake, text, 0);
		this.text = text;
	}

	@Override
	public double baseCost() {
		if (this.text.equals(BIRTHDAY)) {
			return super.baseCost();
		} else if (isMuffinCake(innerCake)) {
			return 8 + super.baseCost();
		} else {
			return 10 + innerCake.baseCost();
		}
	}

	@Override
	public String getDescription() {
		if (isMuffinCake(innerCake)) {
			return super.getDescription() + " with ink on a card";
		} else {
			return super.getDescription();
		}
	}
	
	private boolean isMuffinCake(Cake cake){
		return super.isInnerCake(MuffinCake.class);
	}
}
