package cake;

public abstract class CakeDecorator extends Cake {
	protected Cake innerCake;
	protected String description;
	protected double cost;

	public CakeDecorator(Cake cake, String description, double cost) {
		this.innerCake = cake;
		this.description = description;
		this.cost = cost;
	}

	@Override
	public String getDescription() {
		return String.format("%s, %s", innerCake.getDescription(), this.description);
	}

	@Override
	public double baseCost() {
		return this.cost + innerCake.baseCost();
	}

	protected <T> boolean isInnerCake(Class<T> type) {
		Cake cake = innerCake;
		System.out.println(cake.getClass());
		do {
			if (cake instanceof CakeDecorator) {
				cake = ((CakeDecorator) cake).innerCake;
			} else if (cake.getClass().equals(type)) {
				return true;
			} else {
				return false;
			}
		} while (cake != null);
		return false;
	}
}
