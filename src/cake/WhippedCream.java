package cake;

public class WhippedCream extends CakeDecorator {

	public WhippedCream(Cake cake) {
		super(cake, "Whipped Cream", 4.5);
	}

}
